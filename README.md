# GTK Resources  [![](https://img.shields.io/crates/v/gtk_resources)](https://crates.io/crates/gtk_resources) [![](https://docs.rs/gtk_resources/badge.svg)](https://docs.rs/gtk_resources)

Procedural derive macro for easily loading gtk gresources.

This package depends on the [gtk-rs](https://gtk-rs.org/) project.

## Documentation

Find it on [Docs.rs](https://docs.rs/gtk_resources).

## Example

Add `gtk-resources` to your dependencies of your `Cargo.toml`:

```toml
[dependencies]
gio = "0.9"
gtk = "0.9"
glib = "0.10"
gtk_resources = "0.1.6"
```

And then in your `gtk-rs` project.

```rust
use gtk_resources::UIResource;

#[derive(UIResource, Debug)]
#[resource = "/com/name/project/window.ui"]
struct WindowResource {
    window: gtk::ApplicationWindow,
    display_label: gtk::Label,
    hello_btn: gtk::Button,
}

fn main() {
    gtk::init().unwrap();

    // Register resource bundles
    let res_bytes = include_bytes!("../test/test.gresource");
    let data = glib::Bytes::from(&res_bytes[..]);
    let resource = gio::Resource::from_data(&data).unwrap();
    gio::resources_register(&resource);

    let res = WindowResource::load().unwrap();
    println!("res: {:?}", res);

    // ...
}
```

The file [test/test.gresource.xml](test/test.gresource.xml) defines the resource bundle which is used
by this example.

See [Gio::Resource and glib-compile-resources](https://developer.gnome.org/gtkmm-tutorial/stable/sec-gio-resource.html.en) for more info about resource bundles.

The `#[resource = "/com/name/project/window.ui"]` attribute at the
`WindowResource` struct specifies the path to the corresponding resource.

The field names and types of the UIResource struct have to match the ids
and types from the exported resource objects.
Otherwise the `gtk::Builder` is unable to load them during runtime.

The code gerated for the previous example looks as follows:

```rust
impl UIResource for WindowResource {
    fn load() -> Result<WindowResource, ()> {
        use gtk::BuilderExtManual;
        let b = gtk::Builder::from_resource("/com/name/project/window.ui");
        Ok (WindowResource {
            window: b.get_object::<gtk::ApplicationWindow>("window").ok_or(())?,
            display_label: b.get_object::<gtk::Label>("display_label").ok_or(())?,
            hello_btn: b.get_object::<gtk::Button>("hello_btn").ok_or(())?,
        })
    }
}
```
