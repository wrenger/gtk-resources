extern crate proc_macro;

use proc_macro2::TokenStream;
use quote::{quote, quote_spanned};
use syn::spanned::Spanned;
use syn::{parse_macro_input, Attribute, Data, DeriveInput, Fields, Lit, Meta, MetaNameValue};

#[proc_macro_derive(UIResource, attributes(resource))]
pub fn builder_resources(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input as DeriveInput);

    let name = input.ident;
    let resource_path = parse_resource_attr(&input.attrs).unwrap();
    let object_getters = gen_object_getters(&input.data);

    let expanded = quote! {
        impl UIResource for #name {
            fn load() -> std::result::Result<Self, String> {
                use gtk::prelude::*;
                let b = gtk::Builder::from_resource(#resource_path);
                Ok(#name {
                    #object_getters
                })
            }
        }
    };

    proc_macro::TokenStream::from(expanded)
}

fn parse_resource_attr(attrs: &[Attribute]) -> Result<String, String> {
    if let Some(attr) = attrs.iter().find(|a| a.path.is_ident("resource")) {
        match attr.parse_meta() {
            Ok(Meta::NameValue(MetaNameValue {
                lit: Lit::Str(lit_str),
                ..
            })) => Ok(lit_str.value()),
            _ => Err("Malformed Attribute #[resource = \"/path/to/resource\"]".into()),
        }
    } else {
        Err("Missing Attribute #[resource = \"/path/to/resource\"]".into())
    }
}

fn gen_object_getters(data: &Data) -> TokenStream {
    match *data {
        Data::Struct(ref data) => match data.fields {
            Fields::Named(ref fields) => {
                let getters = fields.named.iter().map(|f| {
                    let name = &f.ident;
                    let res = name.as_ref().unwrap().to_string();
                    let error = format!("Resource `{}` could not be loaded", res);
                    quote_spanned! {f.span()=>
                        #name: b.get_object(#res).ok_or(#error)?
                    }
                });
                quote! {
                    #(#getters,)*
                }
            }
            _ => unimplemented!(),
        },
        _ => unimplemented!(),
    }
}
