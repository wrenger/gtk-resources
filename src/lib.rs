//! This crate defines the UIResource trait and its custom derive.
//!
//! ## Usage
//!
//! First, let's look at the example:
//!
//! ```
//! use gtk_resources::UIResource;
//!
//! #[derive(UIResource, Debug)]
//! #[resource = "/com/name/project/window.ui"]
//! struct WindowResource {
//!     window: gtk::ApplicationWindow,
//!     display_label: gtk::Label,
//!     hello_btn: gtk::Button,
//! }
//!
//! gtk::init().unwrap();
//!
//! // Register resource bundles
//! let res_bytes = include_bytes!("../test/test.gresource");
//! let data = glib::Bytes::from(&res_bytes[..]);
//! let resource = gio::Resource::from_data(&data).unwrap();
//! gio::resources_register(&resource);
//!
//! let res = WindowResource::load().unwrap();
//! println!("res: {:?}", res);
//!
//! // Display window ...
//! ```
//!
//! The file `test/test.gresource.xml` defines the resource bundle which is used
//! by this example.
//!
//! ```xml
//! <gresources>
//!     <gresource prefix="/com/name/project">
//!         <file preprocess="xml-stripblanks">window.ui</file>
//!     </gresource>
//! </gresources>
//! ```
//!
//! See [Gio::Resource and glib-compile-resources](https://developer.gnome.org/gtkmm-tutorial/stable/sec-gio-resource.html.en) for more info about resource bundles.
//!
//! The `#[resource = "/com/name/project/window.ui"]` attribute at the
//! `WindowResource` struct specifies the path to the corresponding resource.
//!
//! The field names and types of the UIResource struct have to match the ids
//! and types from the exported resource objects.
//! Otherwise the `gtk::Builder` is unable to load them during runtime.
//!
//! The code gerated for the previous example looks as follows:
//!
//! ```
//! # pub trait UIResource {
//! #     fn load() -> Result<Self, ()>
//! #     where
//! #         Self: Sized;
//! # }
//!
//! # struct WindowResource {
//! #     window: gtk::ApplicationWindow,
//! #     display_label: gtk::Label,
//! #     hello_btn: gtk::Button,
//! # }
//!
//! impl UIResource for WindowResource {
//!     fn load() -> Result<WindowResource, ()> {
//!         use gtk::prelude::*;
//!         let b = gtk::Builder::from_resource("/com/name/project/window.ui");
//!         Ok (WindowResource {
//!             window: b.get_object("window").ok_or(())?,
//!             display_label: b.get_object("display_label").ok_or(())?,
//!             hello_btn: b.get_object("hello_btn").ok_or(())?,
//!         })
//!     }
//! }
//! ```

#[doc(hidden)]
pub use gtk_resources_derive::*;

/// Trait for `gtk::Builder` resources.
pub trait UIResource {
    /// Loads the resource struct with the `gtk::Builder`
    /// from the corresponding resource bundle.
    fn load() -> Result<Self, String>
    where
        Self: Sized;
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_resource() {
        #[derive(UIResource, Debug)]
        #[resource = "/com/name/project/window.ui"]
        struct TestResource {
            window: gtk::ApplicationWindow,
            display_label: gtk::Label,
            hello_btn: gtk::Button,
        }

        gtk::init().unwrap();

        // Register resource bundles
        let res_bytes = include_bytes!("../test/test.gresource");
        let data = glib::Bytes::from(&res_bytes[..]);
        let resource = gio::Resource::from_data(&data).unwrap();
        gio::resources_register(&resource);

        let res = TestResource::load().unwrap();
        println!("res: {:?}", res);
    }
}
